# README #
* Coding Assignment
* Version 0.4

## Question :     
The attached utf-8 encoded text file contains the favorite musical artists of 1000 users from LastFM. Each line is a list of artists, formatted as follows:

Radiohead,Pulp,Morrissey,Delays,Stereophonics,Blur,Suede,Sleeper,The La's,Super Furry Animals\n
Band of Horses,Iggy Pop,The Velvet Underground,Radiohead,The Decemberists,Morrissey,Television\n
etc.    
**Write a program that takes a file or standard input, and produces a list of pairs of artists which appear TOGETHER in at least fifty different lists. For example, in the above sample, Radiohead and Morrissey appear together twice, but every other pair appears only once. Your program should output the pair list to stdout in the same form as the input (eg Artist Name 1, Artist Name 2\n).

You MAY return an approximate solution, i.e. lists which appear at least 50 times with high probability, as long as you explain why this tradeoff improves the performance of the algorithm.**


##Solution    
###The given zip file contains two folders: 
a) src -> This folder contains two classes I) TopArtistPair.java ii) InputOutCsv.java
b) Data -> This folder contains two files I) ArtistList.csv [Input] ii) TopArtistList.csv[Output]

###Compile & Execution:
1)	Go to src folder on your computer.
2)	Compile & execute files as show in below image.
3)	Enter Input & Output file path along with File name as show in below image.

 
      
###Time | Space Complexity & Logic:
m= Distinct Artist
k = Distinct Artist with users greater than equal to 50
n= Total number of users

1)	First we iterate through Input file creating Hash Map with Artist as Key and Set of users as Value.
Hash Map has Time Complexity of O(1) for insertion, deletion & search
The above data structure has worst case Space Complexity O(m*n)

2)	Then we again Iterate through above created Hash Map and remove Artist who do not have users greater than 49.
This will reduce the number of Artist to 10% of the Artist present in Hash Map. Hence Space Complexity is reduced O(k * n) this also reduces computation for next    operation.3)	At last we Iterate through combination of Artists in the newly created Hash Map and find common users using Sets intersection operation.

###Set Intersection operation has amortize Time Complexity of O(1)
###Therefore, Time Complexity is O(k^2)    

Size k can be optimized by entering Artist which have high probability to form pair.
*
Time Distribution    
Analyzing Data & Designing: 1 Hour    
Coding & Testing: 3 Hours    
Refactoring & Testing: 1 Hour    
Document creation: 30 min    
Total Time = 5.30 + 45min [procrastination & coffee break] = 6.15 Hours    
*