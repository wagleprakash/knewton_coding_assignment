import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

/**
 * Created by prakashwagle on 3/29/16.
 * This is the Main class which contains business logic
 */
public class TopArtistPair {

    HashMap<String,HashSet<Integer>> listOfArtist = null;
    HashMap<String,HashSet<Integer>> listOfPopularArtist = new HashMap<String,HashSet<Integer>>();
    ArrayList<String> listOfTopArtistPair= new ArrayList<String>();

    public static void main(String[] args)
    {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String inputFileLocation="";
        String outputFileLocation="";

        try {
            System.out.print("Please Enter the input file path along with Input Filename: ");
             inputFileLocation = br.readLine();
            System.out.print("Please Enter the output file path along with Output Filename: ");
             outputFileLocation = br.readLine();
        }
        catch (IOException e)
        {
            System.err.println();
        }


        InputOutputCsv io = new InputOutputCsv();
        TopArtistPair tap = new TopArtistPair();

        //Populate list of artist and there users from the given Input File
        tap.listOfArtist=io.readInputCsv(inputFileLocation);
        tap.getPopularArtistPairList();
        io.writeOutputCsv(outputFileLocation,tap.listOfTopArtistPair);


    }

    public void getPopularArtistPairList()
    {
        for(Map.Entry<String, HashSet<Integer>> entry : listOfArtist.entrySet())
        {
            HashSet<Integer> entrySet = entry.getValue();
            if(entrySet.size()>=50)
            {
                // Populate the artist which are present in more than or equal to 50 users favorite list
                // This optimizes the process of finding top Artist Pairs by reducing Memory and Computation requirement.
                listOfPopularArtist.put(entry.getKey(),entrySet);
            }
        }

        Iterator<Map.Entry<String, HashSet<Integer>>> outerIterator = listOfPopularArtist.entrySet().iterator();

        while(outerIterator.hasNext()) {

            Map.Entry<String, HashSet<Integer>> outerEntry = outerIterator.next();

            HashSet<Integer> outerUserSet = outerEntry.getValue();

            Iterator<Map.Entry<String, HashSet<Integer>>> innerIterator = listOfPopularArtist.entrySet().iterator();

            while (innerIterator.hasNext()) {
                Map.Entry<String, HashSet<Integer>> innerEntry = innerIterator.next();

                if (!(outerEntry.getKey().equalsIgnoreCase(innerEntry.getKey()))) // Ignores if both the Artist are same
                {
                    HashSet<Integer> innerUserSet = innerEntry.getValue();
                    HashSet<Integer> commonUserSet = (HashSet) outerUserSet.clone();

                    commonUserSet.retainAll(innerUserSet); // This operation will get the intersection to find the common users among the two given Artist

                    // This condition checks if the pair of Artists have more than or equal to 50 common users
                    if (commonUserSet.size() >= 50) {
                        String topPairOfArtist = outerEntry.getKey() + "," + innerEntry.getKey();
                        listOfTopArtistPair.add(topPairOfArtist);
                        System.out.println(topPairOfArtist);
                    }

                }
            }
            outerIterator.remove(); // Removes a Artist once it has been compared with all other Artist
        }
    }
}
