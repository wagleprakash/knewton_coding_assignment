import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

/**
 * Created by prakashwagle on 3/29/16.
 * This class handles Input Output operations
 */
public class InputOutputCsv {

    private static final String COMMA_DELIMITER = ",";
    private static final String NEW_LINE_SEPARATOR = "\n";

    private HashMap<String,HashSet<Integer>> artistList = new HashMap<String, HashSet<Integer>>();

    public HashMap<String,HashSet<Integer>> readInputCsv(String inputFileLocation)
    {
        Integer userId = 0;
        String line="";
        FileReader fr = null;
        BufferedReader br = null;
        try {
            fr = new FileReader(inputFileLocation);
            br = new BufferedReader(fr);
            while ((line = br.readLine()) != null) {
                userId++; // Creates unique userID for each user
                String[] artistArray = line.split(COMMA_DELIMITER);
                for (int i = 0; i < artistArray.length; i++) {
                    if (artistList.containsKey(artistArray[i])) // This condition checks if given HashMap already contains Artist or not.
                    {
                        HashSet<Integer> userSet = artistList.get(artistArray[i]);
                        userSet.add(userId);
                        artistList.put(artistArray[i], userSet);
                    } else {
                        HashSet<Integer> userSet = new HashSet<Integer>();
                        userSet.add(userId);
                        artistList.put(artistArray[i], userSet);
                    }
                }
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return artistList;
    }

    public void writeOutputCsv(String outputFileLocation, ArrayList<String> listOfArtistPair)
    {
        FileWriter fileWriter = null;

        try
        {
            fileWriter =new FileWriter(outputFileLocation);

            for(String pairOfArtist : listOfArtistPair)
            {
                fileWriter.append(pairOfArtist);
                fileWriter.append(NEW_LINE_SEPARATOR);
            }
        }
        catch (IOException e)
        {
            System.err.println();
        }
        finally {

            try {
                fileWriter.flush();
                fileWriter.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
